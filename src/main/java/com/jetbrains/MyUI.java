package com.jetbrains;

import javax.servlet.annotation.WebServlet;


import Clases.Cliente;
import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.*;
import com.vaadin.ui.Button;
import com.vaadin.ui.Label;
import com.vaadin.ui.TextField;
import com.vaadin.ui.components.grid.SingleSelectionModel;

import java.awt.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * This UI is the application entry point. A UI may either represent a browser window 
 * (or tab) or some part of a html page where a Vaadin application is embedded.
 * <p>
 * The UI is initialized using {@link #init(VaadinRequest)}. This method is intended to be 
 * overridden to add component to the user interface and initialize non-component functionality.
 */
@Theme("mytheme")
public class MyUI extends UI {

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        final VerticalLayout layout = new VerticalLayout();

        Label label = new Label("INGRESANDO CLIENTES");
        layout.addComponent(label);


        HorizontalLayout horizontalLayout = new HorizontalLayout();
        List<Cliente> arreglo = new ArrayList<>();


        final TextField nit = new TextField();
        nit.setCaption("NIT");

        final TextField nombre = new TextField();
        nombre.setCaption("Nombre");

        final TextField apellido = new TextField();
        apellido.setCaption("Apellido");

        final TextField edad = new TextField();
        edad.setCaption("Edad");

        Grid<Cliente> cliente = new Grid<>();
        cliente.addColumn(Cliente::getNit).setCaption("NIT");
        cliente.addColumn(Cliente::getNombre).setCaption("Nombre");
        cliente.addColumn(Cliente::getApellido).setCaption("Apellido");
        cliente.addColumn(Cliente::getEdad).setCaption("Edad");
        cliente.setWidth("800px");

        Button button = new Button("Ingresar");
        button.addClickListener( (Button.ClickEvent e) -> {
            Cliente objeto = new Cliente();
            objeto.setNit(nit.getValue());
            objeto.setNombre(nombre.getValue());
            objeto.setApellido(apellido.getValue());
            objeto.setEdad(edad.getValue());

            arreglo.add(objeto);

            for(int x = 0; x <= arreglo.size() - 1; x++){
                cliente.setItems(arreglo.get(x));

            }

            nit.setValue("");
            nombre.setValue("");
            apellido.setValue("");
            edad.setValue("");

        });
            SingleSelectionModel selection = (SingleSelectionModel) cliente.getSelectionModel();


            Button delSelected = new Button("Eliminar");
                    delSelected.addClickListener( (Button.ClickEvent e ) ->     {

            });




        horizontalLayout.addComponents(nit, nombre, apellido, edad);
        layout.addComponents(button, horizontalLayout, cliente, delSelected);
        setContent(layout);

}



        @WebServlet(urlPatterns = "/*", name = "MyUIServlet", asyncSupported = true)
    @VaadinServletConfiguration(ui = MyUI.class, productionMode = false)
    public static class MyUIServlet extends VaadinServlet {
    }
}

