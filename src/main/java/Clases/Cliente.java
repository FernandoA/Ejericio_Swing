package Clases;

/**
 * Created by Fernando Ambrosio on 04/07/2017.
 */
public class Cliente {

    private String nit;
    private String nombre;
    private String apellido;
    private String edad;

    public Cliente() {
    }

    public Cliente(String nit, String nombre, String apellido, String edad) {
        this.nit = nit;
        this.nombre = nombre;
        this.apellido = apellido;
        this.edad = edad;
    }

    public String getNit() {
        return nit;
    }

    public void setNit(String nit) {
        this.nit = nit;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }
}
